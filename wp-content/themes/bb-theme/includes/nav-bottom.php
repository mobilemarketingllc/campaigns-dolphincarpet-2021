<header class="fl-page-header fl-page-header-primary<?php FLTheme::header_classes(); ?>"<?php FLTheme::header_data_attrs(); ?> itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<div class="fl-page-header-wrap">
		<div class="fl-page-header-container <?php //FLLayout::container_class(); ?>">
			<div class="fl-page-header-row <?php FLLayout::row_class(); ?>">
				<?php if(is_front_page()) { ?>
				<div class="<?php FLLayout::col_classes( array( 'sm' => 12, 'md' => 12 ) ); // @codingStandardsIgnoreLine ?> fl-page-header-logo-col ">
				<?php } else{ ?>
				<div class="<?php FLLayout::col_classes( array( 'sm' => 6, 'md' => 6 ) ); // @codingStandardsIgnoreLine ?> fl-page-header-logo-col">
				<?php } ?>
					<div class="fl-page-header-logo" itemscope="itemscope" itemtype="https://schema.org/Organization">
						<?php if(is_front_page()) { ?><div class="text-center"><?php } ?>
						<a href="https://www.dolphincarpet.com/<?php //echo esc_url( home_url( '/' ) ); ?>" target="_blank" itemprop="url"><?php FLTheme::logo(); ?></a>
						<?php echo FLTheme::get_tagline(); ?>
						<?php if(is_front_page()) { ?></div><?php } ?>
					</div>
				</div>
				<?php if(!is_front_page()) { ?>
				<div class="<?php FLLayout::col_classes( array( 'sm' => 6, 'md' => 6 ) ); // @codingStandardsIgnoreLine ?>">
					<div class="fl-page-header-content">
						<?php FLTheme::header_content(); ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php if(!is_front_page()) { ?>
	<div class="fl-page-nav-wrap">
		<div class="fl-page-nav-container <?php //FLLayout::container_class(); ?>">
			<nav class="fl-page-nav navbar navbar-default navbar-expand-md" aria-label="<?php echo esc_attr( FLTheme::get_nav_locations( 'header' ) ); ?>" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
				<button type="button" class="navbar-toggle navbar-toggler" data-toggle="collapse" data-target=".fl-page-nav-collapse">
					<span><?php FLTheme::nav_toggle_text(); ?></span>
				</button>
				<div class="fl-page-nav-collapse collapse navbar-collapse">
					<?php

					wp_nav_menu(array(
						'theme_location' => 'header',
						'items_wrap'     => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',
						'container'      => false,
						'fallback_cb'    => 'FLTheme::nav_menu_fallback',
					));

					FLTheme::nav_search();

					?>
				</div>
			</nav>
		</div>
	</div>
	<?php } ?>
</header><!-- .fl-page-header -->
