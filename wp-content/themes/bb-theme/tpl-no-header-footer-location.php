<?php

/*
Template Name: No Header/Footer Location
Template Post Type: post, page
*/

add_filter( 'fl_topbar_enabled', '__return_false' );
add_filter( 'fl_fixed_header_enabled', '__return_false' );
add_filter( 'fl_header_enabled', '__return_false' );
add_filter( 'fl_footer_enabled', '__return_false' );
get_header();

?>
<style type="text/css">
.fl-heading .fl-heading-text{color:#000;font-family:Montserrat;font-size:11.4px;letter-spacing:.41px;line-height:13px;text-align:center;font-weight:700}
.fl-row-fixed-width{max-width:900px}
.ginput_price{display:none}
.gfp_big_button .gfield_radio li input[type=radio]{display:none}
.gfp_big_button .gfield_radio label{color:#FFF;font-family:Montserrat!important;font-size:10.8px!important;letter-spacing:.46px!important;line-height:13px!important;text-align:center!important;min-width:150px;width:100%;padding:10px 30px;float:none;background:#0C7062;-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;font-weight:600;margin:0;display:block;-webkit-transition:background-color 300ms ease-out;-moz-transition:background-color 300ms ease-out;transition:background-color 300ms ease-out;-webkit-appearance:none;cursor:pointer;line-height:1;position:relative;text-decoration:none;text-align:center;font-size:1.1em;box-sizing:border-box}
.gfp_big_button .gfield_radio label:hover{background:#0C7062}
.form-one .gform_page_fields>ul>li.gfp_big_button>label,.form-two .gform_page_fields>ul>li.gfp_big_button>label,.form-four .gform_page_fields>ul>li.gfp_big_button>label{color:#000;font-family:Gotham;font-size:30px;font-weight:700;letter-spacing:-.4px;line-height:30px;text-align:center;max-width:100%;width:100%;margin:0 0 50px}
.form-one .gform_page_fields>ul>li.gfp_big_button,.form-two .gform_page_fields>ul>li.gfp_big_button,.form-four .gform_page_fields>ul>li.gfp_big_button{text-align:center}
.form-one .gform_wrapper ul.gform_fields li.gfield,.form-two .gform_wrapper ul.gform_fields li.gfield,.form-four .gform_wrapper ul.gform_fields li.gfield{text-align:center}
.gform_wrapper .gf_progressbar_percentage span{position:absolute;left:50%;line-height:3.3!important;color:#000;font-family:Montserrat;font-size:12.4px;letter-spacing:.48px;text-align:center}
.gform_wrapper .gf_progressbar_percentage span:after{content:" COMPLETE"}
.gform_wrapper .gf_progressbar_percentage{border-radius:0!important;background:#7EA0B7!important;height:40px!important;background-color:#7EA0B7!important}
.form-one .gform_page_footer,.form-two .gform_page_footer,.form-four .gform_page_footer{visibility:hidden;height:0}
ul.gfield_checkbox li input[type="checkbox"],.gform_wrapper ul.gfield_radio li input[type="radio"]{display:none}
ul.gfield_checkbox li label,.gform_wrapper ul.gfield_radio li label{padding:10px}
.form-four ul.gfield_radio li:nth-child(1),.form-four ul.gfield_radio li:nth-child(2){border-right:1px solid #CBCBCB}
.gf_progressbar::after,.gform_wrapper .gf_progressbar{border-radius:0!important;height:40px!important}
.gf_progressbar_wrapper{margin:50px 0 -33px!important}
.gf_progressbar_percentage{border-radius:1px 4px 4px 1px}
.gf_progressbar{padding:0;margin:0}
.gf_progressbar_percentage,.gform_wrapper .gf_progressbar:after{height:40px;border-radius:0;margin-top:-40px!important}
.gf_progressbar_percentage span{line-height:40px}
.gf_progressbar:after{margin-top:-40px}
.noUi-handle::after{left:10px!important}
.noUi-handle::after,.noUi-handle::before{height:10px!important;width:10px!important;background:#D45C29!important;left:5px!important;top:5px!important;border-radius:46px!important}
.noUi-horizontal{height:12px!important}
.noUi-horizontal .noUi-handle{width:22px!important;height:22px!important;left:-17px!important;top:-6px!important}
.noUi-handle{border-radius:99px!important;border:1px solid #9C9C9C!important}
.noUi-target{background:#DEDEDE!important;border-radius:10px!important}
.noUi-connect{background:#0C7062!important}
.slider-display span[class$='val-relation']{margin:.3em!important;font-size:10px!important}
body .gform_wrapper .top_label div.ginput_container input[type="text"],body .gform_wrapper .top_label div.ginput_container select,textarea{border:1px solid #000!important;border-radius:0!important}
.gform_wrapper .gform_page_footer{border-top:0!important}
.gform_wrapper li.gfield.gfield_error.gfield_contains_required div.ginput_container,.gform_wrapper li.gfield.gfield_error.gfield_contains_required label.gfield_label{margin-top:-8px!important}
.gform_wrapper .field_description_below .gfield_description{padding-top:0!important;padding-bottom:10px!important}
.slider>label{margin-top:-30px}
.slider .ginput_container{margin-top:70px!important}
.first-item .ginput_container{margin-top:45px!important}
.noUi-tooltip{border:0!important;bottom:-164%!important;background:transparent!important}
.gfmc-row-1-col-1-of-4{width:40%!important;padding:0 30px 0 20px}
.gfmc-row-1-col-2-of-4{width:60%!important;padding:0 0 0 20px}
.gfmc-row-1-col-3-of-4,.gfmc-row-1-col-4-of-4{width:50%!important}
.form-three div.ginput_container input[type="text"]{width:100%!important}
.gform_button{width:200px;padding:10px 20px!important;background-color:#0C7062!important;border-color:#0C7062!important}
.gform_button:hover{color:#fff!important}
.gform_wrapper .gform_page_footer{text-align:right!important}
.gform_previous_button{visibility:hidden!important}
.form-two .gfield_description,.form-four .gfield_description{color:#000!important;font-family:Gotham!important;font-weight:300!important;line-height:23px!important;font-size:15px!important;width:500px!important;text-align:center!important;margin:0 auto!important;margin-top:-30px!important;display:inline-block!important;margin-bottom:20px!important}
.form-two ul.gfield_radio li label{padding:10px 20px!important;width:360px!important}
.form-four ul.gfield_radio li label{background:#fff}
.form-four .gfp_big_button .gfield_radio label:hover{background:#fff!important}
.form-two .gform_page_fields>ul>li.gfp_big_button>label,.form-four .gform_page_fields>ul>li.gfp_big_button>label{margin:0 0 35px!important}
.gform_wrapper .gf_progressbar{padding:0!important}
.custom-radio .gfield_radio{columns:3;-webkit-columns:3;-moz-columns:3;width:33em}
.custom-radio .gfield_radio li{break-inside:avoid;position:relative;text-align:center;width:100%;padding:0;margin:10px 0}
.custom-radio .gfield_radio li label{margin:0!important;border:2px solid #fff;max-width:100%!important;cursor:none}
.custom-radio .gfield_radio li label .checkbox-img{width:100%;clear:both;text-align:center;margin:0 auto}
.custom-radio .gfield_radio li label p{margin:0 auto;color:#000!important}
.custom-radio .gfield_radio li label p.heading{position:relative;font-size:16px;color:#77170a;text-transform:uppercase;font-weight:bolder;display:inline-block;width:inherit;margin-top:10px}
.custom-radio .gfield_radio li label p.link>a{background:#fff;border:2px solid #000;padding:10px 33px;margin:5px 0 20px;display:inline-block;cursor:pointer;text-decoration:none;color:#000}
.custom-radio .gfield_radio input[type="radio"]{opacity:0;outline:none;z-index:100;width:27px;height:27px;top:0;left:0;position:absolute;appearance:none;cursor:none}
.gform_wrapper ul li.field_description_below div.ginput_container_radio{margin-right:0!important}
.gform_wrapper .field_sublabel_above .ginput_complex.ginput_container label,.gform_wrapper .field_sublabel_above div[class*=gfield_time_].ginput_container label{margin:9px 0 -9px 1px !important;font-weight:700!important;font-size:15px!important}
body .gform_wrapper ul li.gfield{margin-top:7px!important}
.gform_confirmation_message{text-align:center;font-weight:700!important}
@media only screen and (max-width:767px) {
.gform_wrapper .top_label .gfield_label{margin-left:0!important}
.form-two .gfield_description,.form-four .gfield_description{width:300px!important}
.custom-radio .gfield_radio li label{min-height:inherit!important}
.custom-radio .gfield_radio{columns:inherit!important;-webkit-columns:inherit!important;-moz-columns:inherit!important;width:inherit!important}
.gform_page_footer{height:30px}
.form-three .gform_page_footer{height:119px}
.gform_wrapper .gform_page_footer .button.gform_previous_button{height:0}
.fl-row-content{margin:10px}
.fl-node-5c80d022966ee>.fl-row-content-wrap{padding-top:0}
.gform_wrapper ul li.field_description_below div.ginput_container_radio{margin-right:0!important}
.form-two ul.gfield_radio li label{width:300px!important}
.form-four ul.gfield_radio li:nth-child(1),.form-four ul.gfield_radio li:nth-child(2){border-right:0}
}
@media only screen and (min-width:641px) {
.gform_wrapper .gf_progressbar{width:calc(100% + 42px)!important;margin-left:-21px!important;margin-bottom:-53px!important}
}
</style>
<div class="fl-content-full container">
	<div class="row">
		<div class="fl-content col-md-12">
			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
					
					get_template_part( 'content', 'page' );
				endwhile;
			endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>

<script type="text/javascript">
// This is the initial GravityForms binding, it will be lost upon a page change with next/previous
// Thus we make a bind on gform_page_loaded event also

var agentfeesLabel = jQuery(".gf_progressbar_wrapper").detach();
jQuery(".gform_body").after(agentfeesLabel);
var current_page = jQuery("#gform_source_page_number_12").val();
    console.log(current_page);
    if(current_page=="2" || current_page=="4"){
        jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage span").html("66%");
        jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage").attr("style", "width:66%");
    } else if(current_page=="3"){
         jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage span").html("33%");
        jQuery(".gf_progressbar_wrapper .gf_progressbar .gf_progressbar_percentage").attr("style","width:33%");
    }

	jQuery('.gfield_radio input[type=radio]').bind("click", function() {
	    
	    console.log(this);
	    console.log(jQuery(this).val());
	     console.log(jQuery(this).attr("name"));
	    if(jQuery(this).attr("name")== "input_2") {
    	    if(jQuery(this).val() == "Poor" || jQuery(this).val() == "Fair" || jQuery(this).val() == "Average")
    	        jQuery("#gform_target_page_number_12").val("3");  jQuery("#gform_12").trigger("submit",[true]); 
    	    if(jQuery(this).val() == "Good" || jQuery(this).val() == "Excellent")
    	        jQuery("#gform_target_page_number_12").val("2");  jQuery("#gform_12").trigger("submit",[true]); 
	    }
	    if(jQuery(this).attr("name")== "input_7") {
    	    if(jQuery(this).val() == "Proceed to Leave Your Feedback.")
    	        jQuery("#gform_target_page_number_12").val("4");  jQuery("#gform_12").trigger("submit",[true]); 
	    }
	   
	});


</script>