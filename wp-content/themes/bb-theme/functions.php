<?php

/*

WARNING! DO NOT EDIT THEME FILES IF YOU PLAN ON UPDATING!

Theme files will be overwritten and your changes will be lost
when updating. Instead, add custom code in the admin under
Appearance > Theme Settings > Code or create a child theme.

*/

// Defines
define( 'FL_THEME_VERSION', '1.7.1.4' );
define( 'FL_THEME_DIR', get_template_directory() );
define( 'FL_THEME_URL', get_template_directory_uri() );

// Classes
if ( ! class_exists( 'FL_Filesystem' ) ) {
	require_once 'classes/class-fl-filesystem.php';
}
require_once 'classes/class-fl-color.php';
require_once 'classes/class-fl-css.php';
require_once 'classes/class-fl-customizer.php';
require_once 'classes/class-fl-fonts.php';
require_once 'classes/class-fl-layout.php';
require_once 'classes/class-fl-theme.php';
require_once 'classes/class-fl-theme-update.php';
require_once 'classes/class-fi-compat.php';
require_once 'classes/class-fl-shortcodes.php';
require_once 'classes/class-fl-wp-editor.php';

/* WP CLI Commands */
if ( defined( 'WP_CLI' ) ) {
	require 'classes/class-fl-wpcli-command.php';
}

// Theme Actions
add_action( 'after_switch_theme', 'FLCustomizer::refresh_css' );
add_action( 'after_setup_theme', 'FLTheme::setup' );
add_action( 'init', 'FLTheme::init_woocommerce' );
add_action( 'wp_enqueue_scripts', 'FLTheme::enqueue_scripts', 999 );
add_action( 'widgets_init', 'FLTheme::widgets_init' );
add_action( 'wp_footer', 'FLTheme::go_to_top' );
add_action( 'fl_after_post', 'FLTheme::after_post_widget', 10 );
add_action( 'fl_after_post_content', 'FLTheme::post_author_box', 10 );
// Header Actions
add_action( 'wp_head', 'FLTheme::pingback_url' );
add_action( 'fl_head_open', 'FLTheme::title' );
add_action( 'fl_head_open', 'FLTheme::favicon' );
add_action( 'fl_head_open', 'FLTheme::fonts' );

// Theme Filters
add_filter( 'body_class', 'FLTheme::body_class' );
add_filter( 'excerpt_more', 'FLTheme::excerpt_more' );
add_filter( 'loop_shop_columns', 'FLTheme::woocommerce_columns' );
add_filter( 'comment_form_default_fields', 'FLTheme::comment_form_default_fields' );
add_filter( 'woocommerce_style_smallscreen_breakpoint', 'FLTheme::woo_mobile_breakpoint' );

// Theme Updates
add_action( 'init', 'FLThemeUpdate::init' );

// Admin Actions
add_action( 'admin_head', 'FLTheme::favicon' );

// Customizer
add_action( 'customize_preview_init', 'FLCustomizer::preview_init' );
add_action( 'customize_controls_enqueue_scripts', 'FLCustomizer::controls_enqueue_scripts' );
add_action( 'customize_controls_print_footer_scripts', 'FLCustomizer::controls_print_footer_scripts' );
add_action( 'customize_controls_print_styles', 'FLCustomizer::controls_print_styles' );
add_action( 'customize_register', 'FLCustomizer::register' );
add_action( 'customize_save_after', 'FLCustomizer::save' );

// Compatibility
FLThemeCompat::init();


//Code for loading dynamic loaction field for Gravity forms
add_filter( 'gform_field_choice_markup_pre_render_12_28', 'my_custom_population_function', 10, 4);
function my_custom_population_function(  $choice_markup, $choice, $field, $value) {	
 $locations = 	[
					"pinecrest-palmetto-bay" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJ1fwRBpPG2YgRYMMu6p9PBGY",
						"yelp" => "https://www.yelp.com/writeareview/biz/TTmEmM_hecJqF8PAIUfjxg?return_url=%2Fbiz%2FTTmEmM_hecJqF8PAIUfjxg&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"north-miami-aventura" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJtQQHtaSt2YgR0eLlR807VB0",
						"yelp" => "https://www.yelp.com/writeareview/biz/r1lZM0HqgnXYbVj-Ama2fQ?return_url=%2Fbiz%2Fr1lZM0HqgnXYbVj-Ama2fQ&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],	
					"davie-plantation" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJXWHxmIsH2YgR7SMaMZKDkLo",
						"yelp" => "https://www.yelp.com/writeareview/biz/-Wbc2aJ-Y9rusx_ni419Iw?return_url=%2Fbiz%2F-Wbc2aJ-Y9rusx_ni419Iw&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"coral-springs" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJ11fKvBQF2YgRqpd_PAiIY2w",
						"yelp" => "https://www.yelp.com/writeareview/biz/702HVprNwQ1B4h8GuGbfsg?return_url=%2Fbiz%2F702HVprNwQ1B4h8GuGbfsg&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"west-kendall" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJ91E1u_vA2YgRqUvJ95vgVmg",
						"yelp" => "https://www.yelp.com/writeareview/biz/kg-_Pja6iye1DFueox1Abg?return_url=%2Fbiz%2Fkg-_Pja6iye1DFueox1Abg&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"doral-palmetto" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJfwjVrHi52YgRs6p8qpHzp7U",
						"yelp" => "https://www.yelp.com/writeareview/biz/vRsq5L-YBib2JCVDGmocUQ?return_url=%2Fbiz%2FvRsq5L-YBib2JCVDGmocUQ&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"pembroke-pines" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJJ5c5LsSo2YgRFkvESau6hd8",
						"yelp" => "https://www.yelp.com/writeareview/biz/GRl5FiVROiiE41pwP3K0uA?return_url=%2Fbiz%2FGRl5FiVROiiE41pwP3K0uA&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"ft-lauderdale" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJ7VYEf4IB2YgRzpbgQ8NUHYg",
						"yelp" => "https://www.yelp.com/writeareview/biz/_opCC8i_OQet0wkw2vr7EQ?return_url=%2Fbiz%2F_opCC8i_OQet0wkw2vr7EQ&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"deerfield-beach" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJtbwTdqEd2YgRExQ0dA9aEOg",
						"yelp" => "https://www.yelp.com/writeareview/biz/d-DhCrXMXVV3WvqGNU5MCg?return_url=%2Fbiz%2Fd-DhCrXMXVV3WvqGNU5MCg&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						],
					"west-broward" => [
						"google" => "https://search.google.com/local/writereview?placeid=ChIJyZCKHMmm2YgR1qF00B6p9hI",
						"yelp" => "https://www.yelp.com/writeareview/biz/s-eYCBfiWlfTWtainMcsyQ?return_url=%2Fbiz%2Fs-eYCBfiWlfTWtainMcsyQ&source=biz_details_war_button",
						"customer" => "https://www.customerlobby.com/reviews/27317/dolphin-carpet-and-tile/write",
						]					
						
				];
						
	

		$slug = get_post_field('post_name', get_post());
		
		$pattern = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
		$newText = preg_replace($pattern,  $locations[$slug][rgar( $choice, 'value' )], $choice['text']); 
		$new_string = sprintf( '>%s%s<', $newText, "" );
        $choice_markup= str_replace( ">{$choice['text']}<", $new_string, $choice_markup );     
   
    return $choice_markup;   
}

add_filter( 'gform_field_input_12_29', 'display_attachment', 10, 5 );
function display_attachment( $input, $field, $value, $lead_id, $form_id ) {
    //because this will fire for every form/field, only do it when it is the specific form and field
	$name = get_post_field('post_title', get_post());
	$input = '<input name="input_29" id="input_12_29" type="hidden" class="gform_hidden" aria-invalid="false" value="'.$name.'">'; 
    return $input;
}

function myshortcode_title( ){
   return get_the_title();
}
add_shortcode( 'page_title', 'myshortcode_title' );

function myshortcode_link( ){
   return get_permalink();
}
add_shortcode( 'page_link', 'myshortcode_link' );







